﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using JulMar.Windows.Mvvm;
using spRDM.DataModel;
using System.Collections.Generic;

namespace spRDM.viewModel
{
    public abstract class AppViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Computer> _computers;

        public AppViewModel()
        {
            SgaredDelegateCommand = new DelegateCommand(SharedCommand);
            DelegateCommandDelete = new DelegateCommand(CommandDelete);
            VisibleFormAdd = Visibility.Hidden;
            UpdateComputers();
        }

        protected abstract void NewCommand();

        private void SharedCommand(object obj)
        {
            if (obj.ToString() == "new" )
            {
                VisibleFormAdd = Visibility.Visible;
                //var FrmAdd = new FormADD();
                //Children.Add();
                NewCommand();

            }
            if (obj.ToString() == "newCopy")
            {
                VisibleFormAdd = Visibility.Visible;
            }
            
        }

        public DelegateCommand DelegateCommandDelete { get; set; }
        public DelegateCommand SgaredDelegateCommand { get; set; }

        public ObservableCollection<Computer> Computers
        {
            get { return _computers; }
            set
            {
                if (value == _computers)
                    return;

                _computers = value;
                OnPropertyChanged(nameof(Computers));
            }
        }

        private Visibility _visibleFormAdd;
        public Visibility VisibleFormAdd {
            get { return _visibleFormAdd; }
            set {
                if (value == _visibleFormAdd)
                return;

                _visibleFormAdd = value;
                OnPropertyChanged(nameof(VisibleFormAdd));
            }
        }

        

        public event PropertyChangedEventHandler PropertyChanged;

        private void CommandDelete(object obj)
        {
            if (obj == null)
                return;

            using (var context = new RDMContext())
            {
                var id = Int32.Parse((obj as string));
                //Computer it = (Computer)MainWindow.SelectedItem;
                context.Computers.RemoveRange(context.Computers.Where(c => c.ComputerId == id));
                context.SaveChanges();
            }

            UpdateComputers();
        }

        public void UpdateComputers()
        {
            using (var context = new RDMContext())
            {
                Computers = new ObservableCollection<Computer>(context.Computers.OrderBy(p => p.Centr).AsNoTracking());
            }
        }


        public void SearchByName(string computerName, ushort typeSarch)
        {
            if (typeSarch == 0) //Поиск по имени
                using (var context = new RDMContext())
                {
                    Computers =
                        new ObservableCollection<Computer>(
                            context.Computers.Where(p => p.numApt.StartsWith(computerName)));
                }

            if (typeSarch == 1) //Поиск по IP
                using (var context = new RDMContext())
                {
                    Computers =
                        new ObservableCollection<Computer>(
                            context.Computers.Where(p => p.AdressPC.StartsWith(computerName)));
                }

            if (typeSarch == 2) //Поиск по IP
                using (var context = new RDMContext())
                {
                    Computers =
                        new ObservableCollection<Computer>(
                            context.Computers.Where(p => p.Centr.StartsWith(computerName)));
                }

            if (computerName.Length == 0)
                UpdateComputers();
        }

        public void DellComputer(Computer computer)
        {
            if (computer == null)
                return;

            using (var context = new RDMContext())
            {
                context.Computers.Attach(computer);
                context.Computers.Remove(computer);
                context.SaveChanges();
            }

            UpdateComputers();
        }

        public void AddComputer(Computer computer)
        {
            using (var context = new RDMContext())
            {
                context.Computers.Add(computer);
                context.SaveChanges();
            }

            UpdateComputers();
        }

        public void OnPropertyChanged(string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}