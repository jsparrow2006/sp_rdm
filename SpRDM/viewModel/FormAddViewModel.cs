﻿using JulMar.Windows.Mvvm;
using spRDM.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace spRDM.viewModel
{
    public class FormAddViewModel : AppViewModel
    {
        public FormAddViewModel()
        {
            FormAddDelegateCommand = new DelegateCommand(FormAddCommand);
        }

        public DelegateCommand FormAddDelegateCommand { get; set; }

        private void FormAddCommand(object obj)
        {
            if (obj.ToString() == "closeAdd")
            {
                VisibleFormAdd = Visibility.Hidden;
            }
            if (obj.ToString() == "saveAdd")
            {
                if (NameComp == "" || CentrApt == "" || NumApt == "" || CompIP == "")
                {
                    MessageBox.Show("Не заполнены обязательные поля", "Внимание", MessageBoxButton.OK);
                }
                else
                {
                    using (var context = new RDMContext())
                    {
                        context.Computers.Add(new Computer()
                        {
                            NamePC = NameComp,
                            numApt = NumApt,
                            Centr = CentrApt,
                            AdressPC = CompIP,
                            Phones = AptPfone,
                            Rpass = PassRDP,
                            Tpass = PassTV,
                            Vpass = PassVNC,
                            AdressPost = AptAdress,
                            DefaultProgram = CheckedVNC ? "V" : CheckedTV ? "T" : CheckedRDP ? "R" : ""
                        });
                        context.SaveChanges();
                        UpdateComputers();
                        VisibleFormAdd = Visibility.Hidden;
                    }
                }
            }
        }

        protected override void NewCommand()
        {
            CentrApt = "";
            NumApt = "";
            NameComp = "";
            CompIP = "";
            PassVNC = "159";
            PassRDP = "1c";
            PassTV = "pharma22@";
            AptPfone = "";
            AptAdress = "";
            CheckedRDP = false;
            CheckedTV = false;
            CheckedVNC = true;
        }

        private String _centrApt;
        public String CentrApt
        {
            get { return _centrApt; }
            set
            {
                if (value == _centrApt)
                    return;

                _centrApt = value;
                OnPropertyChanged(nameof(CentrApt));
            }
        }

        private String _numApt;
        public String NumApt
        {
            get { return _numApt; }
            set
            {
                if (value == _numApt)
                    return;

                _numApt = value;
                OnPropertyChanged(nameof(NumApt));
            }
        }

        private String _nameComp;
        public String NameComp
        {
            get { return _nameComp; }
            set
            {
                if (value == _nameComp)
                    return;

                _nameComp = value;
                OnPropertyChanged(nameof(NameComp));
            }
        }

        private String _compIP;
        public String CompIP
        {
            get { return _compIP; }
            set
            {
                if (value == _compIP)
                    return;

                _compIP = value;
                OnPropertyChanged(nameof(CompIP));
            }
        }

        private String _passVNC;
        public String PassVNC
        {
            get { return _passVNC; }
            set
            {
                if (value == _passVNC)
                    return;

                _passVNC = value;
                OnPropertyChanged(nameof(PassVNC));
            }
        }

        private String _passRDP;
        public String PassRDP
        {
            get { return _passRDP; }
            set
            {
                if (value == _passRDP)
                    return;

                _passRDP = value;
                OnPropertyChanged(nameof(PassRDP));
            }
        }

        private String _passTV;
        public String PassTV
        {
            get { return _passTV; }
            set
            {
                if (value == _passTV)
                    return;

                _passTV = value;
                OnPropertyChanged(nameof(PassTV));
            }
        }

        private String _aptPfone;
        public String AptPfone
        {
            get { return _aptPfone; }
            set
            {
                if (value == _aptPfone)
                    return;

                _aptPfone = value;
                OnPropertyChanged(nameof(AptPfone));
            }
        }

        private String _aptAdress;
        public String AptAdress
        {
            get { return _aptAdress; }
            set
            {
                if (value == _aptAdress)
                    return;

                _aptAdress = value;
                OnPropertyChanged(nameof(AptAdress));
            }
        }

        private Boolean _checkedVNC;
        private Boolean _checkedRDP;
        private Boolean _checkedTV;

        public Boolean CheckedRDP
        {
            get { return _checkedRDP; }
            set
            {
                if (value == _checkedRDP)
                    return;
                _checkedRDP = value;
                OnPropertyChanged(nameof(CheckedRDP));
            }
        }

        public Boolean CheckedTV
        {
            get { return _checkedTV; }
            set
            {
                if (value == _checkedTV)
                    return;
                _checkedTV = value;
                OnPropertyChanged(nameof(CheckedTV));
            }
        }

        public Boolean CheckedVNC
        {
            get { return _checkedVNC; }
            set
            {
                if (value == _checkedVNC)
                    return;
                _checkedVNC = value;
                OnPropertyChanged(nameof(CheckedVNC));
            }
        }
    }
}
