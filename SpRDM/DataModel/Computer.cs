﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace spRDM.DataModel
{
    public class Computer
    {
        public int ComputerId { get; set; }

        [Required]
        public string Centr { get; set; }
        public string TCver { get; set; }

        [Required]
        public string NamePC { get; set; }
        [Required]
        public string AdressPC { get; set; }
        public string AdressPost { get; set; }
        public string Phones { get; set; }

        [Required]
        public string DefaultProgram { get; set; }
        public string VUser { get; set; }
        public string Vpass { get; set; }
        public string RVUser { get; set; }
        public string Rpass { get; set; }
        public string TUser { get; set; }
        public string Tpass { get; set; }
        public string numApt { get; set; }
        public string typeDB { get; set; }
    }
}
