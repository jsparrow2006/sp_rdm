﻿namespace spRDM.DataModel
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class RDMContext : DbContext
    {
        public RDMContext()
            : base("name=RDMContext")
        {
        }

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public DbSet<Computer> Computers { get; set; }
    }

}