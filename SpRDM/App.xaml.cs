﻿using spRDM.viewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace spRDM
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static AppViewModel ApplicationViewModel { get; set; }

        public App()
        {
            ApplicationViewModel = new FormAddViewModel();
        }
    }
}
