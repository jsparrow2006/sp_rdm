﻿using spRDM.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace spRDM
{
    /// <summary>
    /// Логика взаимодействия для FormADD.xaml
    /// </summary>
    public partial class FormADD : UserControl
    {
        public FormADD()
        {
            InitializeComponent();
        }

        private void btAdd_Copy1_Click(object sender, RoutedEventArgs e)
        {
            AddFullForm.Visibility = Visibility.Hidden;
        }

        private void btAdd_Copy_Click(object sender, RoutedEventArgs e)
        {
            Computer Comp = new Computer
            {
                Centr = textCentr.Text,
                NamePC = textNamePC.Text,
                AdressPC = textIP.Text,
                AdressPost = textAdressPost.Text,
                Phones = textPhones.Text,
                numApt = textNumApt.Text,
                DefaultProgram = "V",
                Vpass = textVNCPass.Text,
                Rpass = textRDPPass.Text,
                Tpass = textTVPass.Text
            };
            if (radioVNC.IsChecked == true)
            {
                Comp.DefaultProgram = "V";
            }
            if (radioRDP.IsChecked == true)
            {
                Comp.DefaultProgram = "R";
            }
            if (radioTV.IsChecked == true)
            {
                Comp.DefaultProgram = "T";
            }

            App.ApplicationViewModel.AddComputer(Comp);

            AddFullForm.Visibility = Visibility.Hidden;
        }

        private void btAdd_Copy_MouseEnter(object sender, MouseEventArgs e)
        {
            
        }
    }
}
