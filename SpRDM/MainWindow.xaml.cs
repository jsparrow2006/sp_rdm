﻿using spRDM.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;

namespace spRDM
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.ApplicationViewModel;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            btClearGroup.Visibility = Visibility.Hidden;
            btClearIP.Visibility = Visibility.Hidden;
            btClearName.Visibility = Visibility.Hidden;
            runWithIp.Visibility = Visibility.Hidden;
            
        }

        private void searchName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (searchName.Text.Length > 0)
            {
                btClearName.Visibility = Visibility.Visible;
            }
            else
            {
                btClearName.Visibility = Visibility.Hidden;
            }
                App.ApplicationViewModel.SearchByName(searchName.Text, 0);  
        }

        private void btClearName_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void btClearName_Click(object sender, RoutedEventArgs e)
        {
            searchName.Text = "";
            btClearName.Visibility = Visibility.Hidden;
        }

        private void searchIP_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (searchIP.Text.Length > 0)
            {
                btClearIP.Visibility = Visibility.Visible;
                runWithIp.Visibility = Visibility.Visible;
            }
            else
            {
                btClearIP.Visibility = Visibility.Hidden;
                runWithIp.Visibility = Visibility.Hidden;
            }

            App.ApplicationViewModel.SearchByName(searchIP.Text, 1);
        }

        private void btClearIP_Click(object sender, RoutedEventArgs e)
        {
            searchIP.Text = "";
            btClearIP.Visibility = Visibility.Hidden;
            runWithIp.Visibility = Visibility.Hidden;
        }

        /*private void btAddCopy_Click(object sender, RoutedEventArgs e)
        {
            Computer it = (Computer)PCList.SelectedItem;
            frmAdd.textAdressPost.Text = it.AdressPost;
            frmAdd.textIP.Text = it.AdressPC;
            frmAdd.textNamePC.Text = it.NamePC;
            frmAdd.textNumApt.Text = it.numApt;
            frmAdd.textPhones.Text = it.Phones;
            frmAdd.textRDPPass.Text = it.Rpass;
            frmAdd.textTVPass.Text = it.Tpass;
            frmAdd.textVNCPass.Text = it.Vpass;
            frmAdd.textCentr.Text = it.Centr;
            if (it.DefaultProgram == "V")
            {
                frmAdd.radioVNC.IsChecked = true;
            }
            if (it.DefaultProgram == "R")
            {
                frmAdd.radioRDP.IsChecked = true;
            }
            if (it.DefaultProgram == "T")
            {
                frmAdd.radioTV.IsChecked = true;
            }
            frmAdd.Visibility = Visibility.Visible;
            frmAdd.AddFullForm.Visibility = Visibility.Visible;
        }*/

        private void btDell_Click(object sender, RoutedEventArgs e)
        {
            //if (MessageBox.Show("Точно удалить из списа", "Внимание", MessageBoxButton.YesNo) == (MessageBoxResult) System.Windows.Forms.DialogResult.OK)
            //{
            //    Computer it = (Computer)PCList.SelectedItem;
            //    App.ApplicationViewModel.DellComputer(it);
            //}
        }

        private void TextBlock_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            //App.ApplicationViewModel.SearchByName(PCList.SelectedItems., 2);
        }
    }
}
